const csv = require('csv-parser')
const fs = require('fs')
const results = [];
const result1=[];
//match_id,inning,batting_team,bowling_team,over,ball,batsman,non_striker,bowler,is_super_over,wide_runs,bye_runs,
//legbye_runs,noball_runs,penalty_runs,batsman_runs,extra_runs,total_runs,player_dismissed,dismissal_kind,fielder
fs.createReadStream('../data/matches.csv')
  .pipe(csv())
  .on('data', function(data){
    if(data.season==="2016"){
        results.push(data.id)
    }
  })
  .on('end', () => {
    fs.createReadStream('../data/deliveries.csv')
    .pipe(csv())
    .on('data', (data)=>{
        result1.push(data)
    }).on('end', () => {
        const newresult=results.reduce((prev,current)=>{
            for(let i in result1){
                if(current===result1[i].match_id){
                    if(prev[result1[i].batting_team]===undefined){
                        prev[result1[i].batting_team]=Number(result1[i].extra_runs);
                    }
                    else{
                        prev[result1[i].batting_team]+=Number(result1[i].extra_runs);
                    }
                }
            }
            return prev;

        },{})
        fs.writeFileSync("../public/output/3-extra-run-conceded-per-team-206.json",JSON.stringify(newresult),'utf8',function(err){console.log(err);});
    })
    
  });


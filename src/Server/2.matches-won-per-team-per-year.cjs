const { match } = require('assert');
const csv = require('csv-parser')
const fs = require('fs')
const results = [];
const obj={

};

fs.createReadStream('../data/matches.csv')
  .pipe(csv())
  .on('data', (data) => results.push(data))
  .on('end', () => {

   let matchWon= results.reduce((previousseason, currentseason)=> {
      const year=currentseason.season;
      const matchwinner=currentseason.winner;

      if(previousseason[year]===undefined){
        previousseason[year]={};
      }
      if(matchwinner!==""){
        if(previousseason[year][matchwinner]==undefined){
          previousseason[year][matchwinner]=1;
        }
        else{
          previousseason[year][matchwinner]++;
        }
        }
      return previousseason;
  },{})
  fs.writeFileSync("../public/output/2-matches-won-per-team-per-year.json",JSON.stringify(matchWon),'utf8',function(err){console.log(err);});


  });
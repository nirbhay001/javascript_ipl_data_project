const { match } = require('assert');
const csv = require('csv-parser')
const fs = require('fs')
const results = [];
const obj={

};

fs.createReadStream('../data/matches.csv')
  .pipe(csv())
  .on('data', (data) => results.push(data))
  .on('end', () => {

   const winner=results.reduce((previousseason, currentseason)=>{
        const tossWinner=currentseason.toss_winner;
        const matchWinner=currentseason.winner;
        if(tossWinner==matchWinner){
            if(previousseason[matchWinner]===undefined){
                previousseason[matchWinner]=1;
            }
            else{
                previousseason[matchWinner]++;
            }
        }
        return previousseason;
   },{});

   fs.writeFileSync("../public/output/5-number-of-each-team-won-toss-and-matches.json",JSON.stringify(winner),'utf8',function(err){console.log(err);});

  });
const { match } = require('assert');
const csv = require('csv-parser')
const fs = require('fs')
const results = [];
const obj={

};

fs.createReadStream('../data/matches.csv')
  .pipe(csv())
  .on('data', (data) => results.push(data))
  .on('end', () => {
    const highesWinnerPlayer=results.reduce((prev,current)=>{
        if(prev[current.season]===undefined){
            prev[current.season]={};
        }
        else{
            if(prev[current.season][current.player_of_match]===undefined){
                prev[current.season][current.player_of_match]=1;
            }
            else{
                prev[current.season][current.player_of_match]++;
            }
        }
        return prev;
    },{})
    const newobj={};
    for(let i in highesWinnerPlayer){
        newobj[i]=Object.fromEntries(Object.entries(highesWinnerPlayer[i]).sort(([,a],[,b])=>{
            return b-a;
            
        }));
    }
    const playerMatchData={};

    for(let i in highesWinnerPlayer){
        playerMatchData[i]=Object.fromEntries(Object.entries(highesWinnerPlayer[i]).sort((a,b)=>{
            return b[1]-a[1];
        }).slice(0,1));
    }
  fs.writeFileSync("../public/output/6-highest-number-won-player-of-match.json",JSON.stringify(playerMatchData),'utf8',function(err){console.log(err);});
});

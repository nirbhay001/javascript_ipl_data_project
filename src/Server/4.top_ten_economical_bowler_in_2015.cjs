const csv = require('csv-parser')
const fs = require('fs')
const results = [];
const result1=[];

fs.createReadStream('../data/matches.csv')
  .pipe(csv())
  .on('data', function(data){
    if(data.season==="2015"){
        results.push(data.id)
    }
  })
  .on('end', () => {
    fs.createReadStream('../data/deliveries.csv')
    .pipe(csv())
    .on('data', (data)=>{
        result1.push(data)
    }).on('end', () => {
        let runs= extraRun(results,result1);
        fs.writeFileSync("../public/output/4-top-ten-economical-bowler-2015.json",JSON.stringify(runs),'utf8',function(err){console.log(err);});

    })
    
  });

//match_id,inning,batting_team,bowling_team,over,ball,batsman,non_striker,bowler,is_super_over,wide_runs,bye_runs,legbye_runs,noball_runs,penalty_runs,batsman_runs,extra_runs,total_runs,player_dismissed,dismissal_kind,fielder
  function extraRun(array,arrayofobjects){
    let obj={};
    let runs;
    let ball;
    let bowler;
    for(let i=0; i<array.length;i++){
        for(let j=0;j<arrayofobjects.length;j++){
            if(array[i]==arrayofobjects[j].match_id){
                if(obj[arrayofobjects[j].bowler]===undefined){
                    obj[arrayofobjects[j].bowler]={
                        run : Number(arrayofobjects[j].total_runs),
                        ball: 1
                    };
                }else{
                    obj[arrayofobjects[j].bowler]['run']+=Number(arrayofobjects[j].total_runs);
                    obj[arrayofobjects[j].bowler]['ball']+=1;
                }
            }
        }
    }

    for(let i in obj){
        obj[i]["Economy"]=(Number(obj[i].run)/(Number(obj[i].ball)/6)).toFixed(2);

    }
   let sortedData = Object.entries(obj).sort((a,b)=>{
        return Object.entries(a)[1][1].Economy- Object.entries(b)[1][1].Economy;
        
    })

    const newdata=sortedData.slice(0,10);
    const topTen=Object.fromEntries(newdata);
    return topTen;

}



const csv = require('csv-parser')
const fs = require('fs')
const results = [];

fs.createReadStream('../data/deliveries.csv')
  .pipe(csv())
  .on('data', (data) => results.push(data))
  .on('end', () => {
    let obj={};
    for(let i in results){
      let  bowler = results[i].bowler;
      let  batsman = results[i].batsman;
      if(results[i].dismissal_kind !== 'run out' && results[i].dismissal_kind!==""){
        if(obj[bowler]===undefined){
          obj[bowler]={};
          obj[bowler][batsman]=1;
        }
        else{
          if(obj[bowler][batsman]===undefined){
            obj[bowler][batsman]=1;
          }
          else{
            obj[bowler][batsman]++;
          }
        }
      }
    }
    let sortobj={};
    for(let i in obj){
      sortobj[i]=Object.fromEntries(Object.entries(obj[i]).sort((a,b)=>{
        return b[1]-a[1];
      }).slice(0,1));
    }
      const bowlerdata=Object.fromEntries(Object.entries(sortobj).sort((a,b)=>{
        return Object.entries(Object.entries(Object.entries(b)[1])[1][1])[0][1]-Object.entries(Object.entries(Object.entries(a)[1])[1][1])[0][1];
      }).slice(0,1));
      fs.writeFileSync("../public/output/8-highest-number-of-times-player-dismissed-by-another-player.json",JSON.stringify(bowlerdata),'utf8',function(err){console.log(err);});

});



































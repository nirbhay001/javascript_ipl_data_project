const csv = require('csv-parser')
const fs = require('fs')
const results = [];

fs.createReadStream('../data/matches.csv')
  .pipe(csv())
  .on('data', (data) => results.push(data))
  .on('end', () => {
    let obj={};
    for(let key in results){
      if(obj[results[key].season]===undefined){
        obj[results[key].season]=1;
      }
      else{
        obj[results[key].season]++;
      }
    }
   fs.writeFileSync("../public/output/1-matches-per-year.json",JSON.stringify(obj),'utf8',function(err){console.log(err);});
});



const csv = require('csv-parser')
const fs = require('fs')
const results = {};
const result1=[];

fs.createReadStream('../data/matches.csv')
  .pipe(csv())
  .on('data', function(data){
    let seasons=data.season
    if(!results[seasons]){
        results[seasons]=[data.id];
    }else{
        results[seasons].push(data.id)
    }
    
  })
  .on('end', () => {
    fs.createReadStream('../data/deliveries.csv')
    .pipe(csv())
    .on('data', (data)=>{
        result1.push(data)
    }).on('end', () => {
        console.log(results);
        const matchData= toCallinside(results,result1);
        fs.writeFileSync("../public/output/7.strike-rate-of-player-in-every-season.json",JSON.stringify(matchData),'utf8',function(err){console.log(err);});

    })
    
  });

//atch_id,inning,batting_team,bowling_team,over,ball,batsman,non_striker,bowler,is_super_over,wide_runs,bye_runs,legbye_runs,noball_runs,penalty_runs,batsman_runs,extra_runs,total_runs,player_dismissed,dismissal_kind,fielder


function toCallinside(match,result){
    let obj={};
    let ball;
    for(let matchKey in match){
        for(let matchdata of match[matchKey]){
            for(let d in result){
                if(matchdata===result[d].match_id){
                    if(obj[matchKey]===undefined){
                        obj[matchKey]={};
                    }
                    else{
                        if(obj[matchKey][result[d].batsman]===undefined){
                            obj[matchKey][result[d].batsman]={
                                run:Number(result[d].total_runs),
                                ball:1
                            };
                        }
                        else{
                            obj[matchKey][result[d].batsman].run+=Number(result[d].total_runs);
                            obj[matchKey][result[d].batsman].ball+=1;
                        }
                    }
                }
            }
        
        }
    }
    for(let i in obj){
        for(let j in obj[i]){
            obj[i][j]["Economy"]=Number((obj[i][j].run/obj[i][j].ball)*100).toFixed(2);

        }
    }
    return obj;
}

const csv = require('csv-parser')
const fs = require('fs')
const results = [];

fs.createReadStream("../data/deliveries.csv")
  .pipe(csv())
  .on('data', (data) => results.push(data))
  .on('end', () => {

   let  obj={};
   let obj1=[]
    for(let key in results){
        if(results[key].is_super_over=="0"){
            continue;
        }
        else{
            if(obj[results[key].bowler]===undefined){
                obj[results[key].bowler]={
                    run:Number(results[key].total_runs),
                    ball:1
                }
            }
            else{
                obj[results[key].bowler].run+=Number(results[key].total_runs);
                obj[results[key].bowler]['ball']++;

            }
        }

    }
    for(let i in obj){

        obj[i]["Economy"]=Number(((obj[i].run)/(((obj[i].ball)/6))));
    }
    
    const sortdata=Object.entries(obj).sort((a,b)=>{
        return Object.entries(a)[1][1].Economy-Object.entries(b)[1][1].Economy;
    })
    const bowler=sortdata.slice(0,1);
    const bestbowler=Object.fromEntries(bowler);
    fs.writeFileSync("../public/output/9-fins-the-bowler-with-the-best-economy-in-super-over.json",JSON.stringify(bestbowler),'utf8',function(err){console.log(err);})
    
  });